# Emotion Image Maker

これは、Pramook Khungurn氏の開発された「Talking Head(?) Anime from A Single Image 3: Now the Body Too」およびそのデモコード実装である「talking-head-anime-3-demo」([https://github.com/pkhungurn/talking-head-anime-3-demo](https://github.com/pkhungurn/talking-head-anime-3-demo)) の派生として作られたソフトウェアプログラムです。

イラストを読み込ませることで、6枚の表情差分を作成します。

## 利用規約
本ソフトウェアは、MITライセンスです。

Emotion Image Maker

Copyright (c) 2023 pale_color

## 動作環境
ソースコードを用いる場合は「[talking-head-anime-3-demo](https://github.com/pkhungurn/talking-head-anime-3-demo)」の動作環境を構築したうえで、``EmotionImage_Maker.py``を``tha3/app/``の下に配置してください。

## クイックスタート
* 次のコマンドにより実行して下さい。
```
python tha3/app/EmotionImage_Maker.py
```

起動後、「Load Image」ボタンを押して画像ファイルを読み込むと、その画像ファイルのあるディレクトリ下に``（拡張子を除いた画像ファイル名）_EmotionImage``というディレクトリが作成され、その中に差分が作成されます。

通常はオプションを指定する必要はありませんが、必要な場合は以下のオプションが使用可能です。

``--model``は使用する機械学習モデルを指定するコマンドラインオプションです。モデルは4種類が用意されており、``standard_float``, ``separable_float``, ``standard_half``, ``separable_half``が指定できます。いずれも機能はほぼ同じですが、サイズ、RAM使用量、動作速度、負荷、精度などが異なります。試行の上で適したものを使用してください。なお、オプションを指定しない場合はデフォルトで``standard_float``のモデルが使用されます。

``--timer``はイラスト生成の間隔を指定するオプションです。値はミリ秒(ms)単位で、5～2000の範囲の整数で指定します。5未満、または2000を超える値を指定した場合はこの範囲に丸められます。なお、オプションを指定しない場合はデフォルトで``20``とみなされます。

## 権利表記
* 本ソフトウェアはMITライセンスでリリースされた「talking-head-anime-3-demo」([https://github.com/pkhungurn/talking-head-anime-3-demo](https://github.com/pkhungurn/talking-head-anime-3-demo))を使用しています。
* talking-head-anime-3-demoの学習済みモデルは、クリエイティブコモンズ 表示 4.0 国際 (CC BY 4.0) ライセンスのもとでリリースされています。

> Copyright (c) 2022 Pramook Khungurn
